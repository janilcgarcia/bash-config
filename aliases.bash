#!/bin/bash

alias la="ls -la"
alias ll="ls -l"

cdp() {
    cd $(cdproject -p "$1")
}

_cdp_complete() {
    COMPREPLY=( $(cdproject "$2") )
}

complete -F _cdp_complete cdp
