module Main (main) where

import Data.Functor ((<&>))
import Control.Monad (mapM_, filterM, mapM)

import qualified Data.Map.Lazy as M
import qualified Data.List as L
import Data.Maybe (mapMaybe, fromMaybe, listToMaybe)

import Text.FuzzyFind (bestMatch, score, Alignment)

import System.FilePath
import System.Directory (listDirectory, doesDirectoryExist)

import System.Environment (getArgs, getEnv)

projectDefaultType :: String
projectDefaultType = "code"

projectDirectories :: FilePath -> M.Map String FilePath
projectDirectories projectHome = M.fromList
  [ ("code", projectHome </> "code")
  , ("config", projectHome </> "config")
  ]

projectFindDirectory :: FilePath -> String -> IO (String)
projectFindDirectory home t = do
  case (M.lookup t . projectDirectories $ home) of
    Just dir -> pure dir
    Nothing -> fail $ "Project type not known: " ++ t

genPath :: FilePath -> FilePath -> IO ()
genPath baseDir path = do
  putStrLn $ baseDir </> path

genSuggestions :: FilePath -> FilePath -> IO ()
genSuggestions baseDir path = do
  let segments = case splitPath path of
        [] -> [""]
        x -> reverse x

      basePath = joinPath . tail $ segments
      fullPath = baseDir </> basePath
      search = head segments

  (siblings, selected) <- searchMatch search fullPath

  if null selected
    then printMatches basePath
           =<< (mapM (subMatch search fullPath) siblings <&> L.concat)
    else printMatches basePath selected

  where
    printMatches :: FilePath -> [(FilePath, Int)] -> IO ()
    printMatches path = mapM_ (putStrLn . (path </>) . fst) . L.sortOn snd

    subMatch :: String -> FilePath -> String -> IO [(FilePath, Int)]
    subMatch query baseDir baseSibling = do
      matches <- searchMatch' query $ baseDir </> baseSibling
      return . fmap addBase $ matches

      where
        addBase (matchedPath, score) = (baseSibling </> matchedPath, score)

    searchMatch' :: String -> FilePath -> IO [(FilePath, Int)]
    searchMatch' query = fmap snd . searchMatch query

    searchMatch :: String -> FilePath -> IO ([FilePath], [(FilePath, Int)])
    searchMatch query baseDir = do
      siblings <- listDirectory baseDir
                    >>= filterM (doesDirectoryExist . (baseDir </>))

      return (siblings, mapMaybe (matchWithScore query) $ siblings)
    
    matchWithScore :: String -> String -> Maybe (String, Int)
    matchWithScore search string =
      fmap makePair match
      where
        match = bestMatch search string

        makePair :: Alignment -> (String, Int)
        makePair = (,) string . score
  

splitBy :: (a -> Bool) -> [a] -> [[a]]
splitBy _ [] = [[]]
splitBy pred xs =
  case L.findIndex pred xs of
    Just index -> let (first, (_:rest)) = L.splitAt index xs
                  in first : splitBy pred rest

    Nothing -> [xs]

split :: Eq a => a -> [a] -> [[a]]
split el = splitBy (== el)

main :: IO ()
main = do
  args <- getArgs

  projectHome <- getEnv "HOME" <&> (</> "projects")

  let genPathFlag = "-p"
      genPathMode = genPathFlag `elem` args
      genSuggestionMode = not genPathMode

      cleanArgs = filter (/= genPathFlag) args

      mode = if genPathMode
             then genPath
             else genSuggestions

      pathArg = fromMaybe "" . listToMaybe $ cleanArgs

  (baseDir, path) <-
    case split ':' pathArg of
      [projectType, path] ->
        projectFindDirectory projectHome projectType
          >>= return . flip (,) path
      [path] -> projectFindDirectory projectHome projectDefaultType
                  >>= return . flip (,) path
      _ -> fail $ "Invalid path: " ++ pathArg

  mode baseDir path
